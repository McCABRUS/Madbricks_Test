﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madbricks
{
    public class MBLevelManager : MonoBehaviour
    {
        public bool firstLoad = true;
        //Singleton
        private static MBLevelManager _levelInstance;
        public static MBLevelManager LevelInstance
        {
            get
            {
                if (_levelInstance == null)
                {
                    _levelInstance = FindObjectOfType<MBLevelManager>();
                    if (_levelInstance == null)
                    {
                        GameObject instance = new GameObject();
                        instance.name = typeof(MBLevelManager).Name;
                        _levelInstance = instance.AddComponent<MBLevelManager>();
                        DontDestroyOnLoad(instance);
                    }
                }
                return _levelInstance;
            }
        
        }

        private void Awake() 
        {
            if (_levelInstance == null)
            {
                _levelInstance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

        } 

        public event System.Action<LevelStage> OnLevelStateChanged;

        public LevelStage LevelStage { get; private set; } = LevelStage.setup;

        public void StartGame()
        {
            SetLevelState(LevelStage.playing);
        }

        public void Win()
        {
            if (LevelStage != LevelStage.playing) { return; }
            SetLevelState(LevelStage.win);
        }

        public void Lose()
        {
            if (LevelStage != LevelStage.playing) { return; }
            SetLevelState(LevelStage.lose);
        }

        private void SetLevelState(LevelStage state)
        {
            LevelStage = state;
            OnLevelStateChanged?.Invoke(LevelStage);
        }
    }

    public enum LevelStage
    {
        setup = 0,
        playing = 1,
        win = 2,
        lose = 3
    }
}