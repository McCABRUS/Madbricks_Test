﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madbricks
{
    public class MBCharacterStateJetpack : MBCharacterStateBase
    {
        public MBCharacterStateJetpack(MBCharacter character) : base(character)
        { }

        public override void ProcessInput(Vector2 direction, bool specialPressed)
        {
            timer += Time.deltaTime;
            //Debug.Log("Jetpack time left: " + (jetpackTime - timer));
            if (timer >= jetpackTime)
            {
                Debug.Log("Jetpack time is over");
                UpdateState();
            }
            //Move Character 4 directions       
            character.GetComponent<Rigidbody2D>().AddForce(direction);
                
            
            
        }

        public override void UpdateState()
        {
            //TODO: Charge Special
            character.SetState(new MBCharacterStateGrounded(character));
        }
    }
}