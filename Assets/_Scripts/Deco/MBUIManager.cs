﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madbricks
{
    public class MBUIManager : MonoBehaviour
    {
        public GameObject resultText;
        public GameObject UI;

        void Start() {
            UI.SetActive(true);
        }


        void Update()
        {
            if (MBLevelManager.LevelInstance.LevelStage == LevelStage.win)
            {
                ShowResultWindow("won");
            }
            else if (MBLevelManager.LevelInstance.LevelStage == LevelStage.lose)
            {
                ShowResultWindow("lost");
            }
        }

        public void StartGame()
        {
            MBLevelManager.LevelInstance.StartGame();
        }

        public void ShowResultWindow(string result)
        {
            UI.SetActive(true);
            resultText.SetActive(true);
            resultText.GetComponent<TMPro.TextMeshProUGUI>().text = "You have " + result + "!";

        }
    }

}


