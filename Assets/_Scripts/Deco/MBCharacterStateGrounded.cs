﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madbricks
{
    public class MBCharacterStateGrounded : MBCharacterStateBase
    {
        public MBCharacterStateGrounded(MBCharacter character) : base(character)
        {
            if (MBLevelManager.LevelInstance.firstLoad == true)
            {
                holdTime = 0.0f;
                MBLevelManager.LevelInstance.firstLoad = false;
            }
        }

        public override void ProcessInput(Vector2 direction, bool specialPressed)
        {
            timer += Time.deltaTime;
            //Debug.Log("time left untul you can use Jetpack once again: " + (holdTime - timer));
            if(specialPressed == true && timer >= holdTime)
            {        
                 UpdateState();    
            }
            
            //Move Character right or left
            if (direction != Vector2.up * MBInputManager.InputInstance.speed && direction != Vector2.down * MBInputManager.InputInstance.speed)
            {
                character.transform.Translate(direction*Time.deltaTime, Space.World);
                
            }
            
            
        }

        public override void UpdateState()
        {
            //Charge Special
            character.SetState(new MBCharacterStateJetpack(character));
            
        }
        
    }

}