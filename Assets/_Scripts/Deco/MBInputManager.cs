﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madbricks
{
    public class MBInputManager : MonoBehaviour
    {
        public int speed = 3;

        //Singleton
        private static MBInputManager _inputInstance;
        public static MBInputManager InputInstance
        {
            get
            {
                if (_inputInstance == null)
                {
                    _inputInstance = FindObjectOfType<MBInputManager>();
                    if (_inputInstance == null)
                    {
                        GameObject instance = new GameObject();
                        instance.name = typeof(MBInputManager).Name;
                        _inputInstance = instance.AddComponent<MBInputManager>();
                        DontDestroyOnLoad(instance);
                    }
                }
                return _inputInstance;
            }

        }

        private void Awake()
        {
            if (_inputInstance == null)
            {
                _inputInstance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

        }
        public Vector2 Direction { get; private set; }

        public bool SpecialPressedThisFrame => Input.GetKeyDown(KeyCode.Space);

        private void Update()
        {
            UpdateDirection();
        }

        public Vector2 UpdateDirection()
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                Direction = Vector2.right;
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                Direction = Vector2.left;
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                Direction = Vector2.up;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                Direction = Vector2.down;
            }
            else
            {
                Direction = Vector2.zero;
            }

            return Direction * speed;
        }
    }
}