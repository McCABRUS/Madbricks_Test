﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madbricks
{
    public abstract class MBCharacterStateBase
    {
        public float timer = 0f;
        public float holdTime = 5.0f; // 5 seconds
        public float jetpackTime = 10.0f; // 10 seconds
        protected MBCharacter character;

        public MBCharacterStateBase(MBCharacter character)
        {
            this.character = character;
        }

        public abstract void UpdateState();
        public abstract void ProcessInput(Vector2 direction, bool specialPressed);
    }
}