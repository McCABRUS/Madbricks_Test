﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madbricks
{
    public class MBCharacter : MonoBehaviour
    {
        bool endedGame = false;
        private MBCharacterStateBase currentState;

        public void SetState(MBCharacterStateBase state)
        {
            currentState = state;
        }

        private void Awake()
        {
            HandleLevelStageChanged(LevelStage.playing);
        }

        private void Update()
        {
            //currentState.UpdateState(); Not usable at this point

            //handle state input
            currentState.ProcessInput(MBInputManager.InputInstance.UpdateDirection(), MBInputManager.InputInstance.SpecialPressedThisFrame);
        }

        private void HandleLevelStageChanged(LevelStage stage)
        {
            if (stage == LevelStage.playing)
            {
                SetState(new MBCharacterStateGrounded(this));
            }
            else
            {
                SetState(new MBCharacterStateDisabled(this));
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "RedPlatform" && endedGame == false)
            {
                Debug.Log("Lost Game");
                MBLevelManager.LevelInstance.Lose();
                HandleLevelStageChanged(LevelStage.lose);
                endedGame = true;
            }

            if (collision.gameObject.tag == "GreenPlatform" && endedGame == false)
            {
                Debug.Log("Won Game");
                MBLevelManager.LevelInstance.Win();
                HandleLevelStageChanged(LevelStage.win);
                endedGame = true;
            }
        }
    }
}